/* ==== IMPORT PARAMS ==== */
import del from 'del';
/* ==== ----- ==== */

module.exports = () =>
	() => del([
		`${__dirname}\\..\\public`,
		`${__dirname}\\..\\development\\tmp`,
		`${__dirname}\\..\\development\\components\\plugins`
	], { read: false });
