module.exports = class AddFetchServerData {
	constructor(enter, data, exit) {
		this.enter = enter;
		this.exit = exit;
		this.server = data.server;
		this.attr = data.selector;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.enter}`);
		// const output = document.querySelector(`${this.exit}`);
		if (elem) {
			this.constructor.info();

			const init = {
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				mode: 'cors',
				cache: 'default'
			};

			fetch(`./${this.server}`, init)
				.then((response) => {
					response.headers.get('Content-Type');
					return response.json();
				})
				.then((data) => {
					const select = document.querySelectorAll(`[${this.attr}]`);
					const arr = [...select].map(i => (Object.assign(i.dataset)));
					const newArr = arr.map(x => x.galleryInit);
					newArr.map(i => (
						document.querySelector(`[${this.attr}="${i}"]`).innerText = `${data.one[i] || 'null'}`));
				})
				.then(() => {
					document.querySelector('.gallery__info').classList.add('no-before');
				})
				.catch((error) => {
					console.log(`Ouch! Fetch error: \n ${error.message}`);
				});
		}
	}
};
