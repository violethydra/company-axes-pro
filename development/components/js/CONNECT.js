// ============================
//    Name: index.js
// ============================

// import AddMoveScrollUP from './modules/moveScrollUP';
// import AddFetchServerData from './modules/fetchServerData';
import AddGeneratorTable from './modules/generatorTable';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	// new AddMoveScrollUP('.js__moveScrollUP').run();

	// new AddFetchServerData('.js__galleryData', {
	// 	server: 'my-server.json',
	// 	selector: 'data-gallery-init'
	// }).run();
	
	new AddGeneratorTable({
		server: 'server-hr-data.json',
		selector: '.js__generatorEnter'
	}).run();
	
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
 