@'
-------------------------------
- Remove template files
-------------------------------
'@

# Params
$MyFolders = 'node_modules', 'public'
$MyFiles = 'yarn.lock'
$MySingleTmp = 'development/tmp'

# Actions
Remove-Item -Path $MyFolders -Recurse -ErrorAction Ignore 
Remove-Item -Path $MyFiles -Recurse -ErrorAction Ignore 
Remove-Item -Path $MySingleTmp -Recurse -ErrorAction Ignore 

# End
@'
-------------------------------
- DONE!!!
-------------------------------
'@
# Exit to any button
Write-Host -NoNewLine 'Press any key to continue...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

