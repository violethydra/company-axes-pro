Описание проекта
===

## СБОРКА ПРОЕКТА
	"name": "PACKAGE",
	"version": "1.2.3",

#### СТРУКТУРА
	- development → сборочная область для формирования цельного кода
	- gulp_modules → gulp разработанный через модульную иньекцию прямиком в продукт
	- public → сгенерированная итоговая область
	- __resource → хранение ТЗ и PSD макетов
	- Readme.md → git-редми файл
	- package.json → список зависимостей в проекте
	- PROJECT.sublime-workspace → рабочая область для SublimeText
	- PROJECT.sublime-project → рабочая область для SublimeText
	- gulpfile.babel.js → сборочные таски
	- .gitattributes → корректировка для git
	- .gitconfig → бекап алиасов
	- .gitignore → игнорфайл для git
	- Contributing.md → описание
	- Changelog → список изменений
	- License → лицензия
	- .gitlab-ci.yml → тесты для gitlab
	- .eslintrc → конфиг для линтинга
	- .eslintignore → игнор лист для линтера
	- .tidyconfig → конфиг для линтинга html
	- .babelrc → мост для babel

#### HTML
	- Include → layouts / встраивание частей страниц в index.html

#### CSS
	- Stylus → препроцессор с расширением .styl | .stylus

#### JS
	- Webpack → javascript собирается в связке с Babel