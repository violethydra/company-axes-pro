/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_generatorTable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/generatorTable */ "./development/components/js/modules/generatorTable.js");
/* harmony import */ var _modules_generatorTable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_generatorTable__WEBPACK_IMPORTED_MODULE_0__);
// ============================
//    Name: index.js
// ============================
// import AddMoveScrollUP from './modules/moveScrollUP';
// import AddFetchServerData from './modules/fetchServerData';


var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true); // new AddMoveScrollUP('.js__moveScrollUP').run();
  // new AddFetchServerData('.js__galleryData', {
  // 	server: 'my-server.json',
  // 	selector: 'data-gallery-init'
  // }).run();

  new _modules_generatorTable__WEBPACK_IMPORTED_MODULE_0___default.a({
    server: 'server-hr-data.json',
    selector: '.js__generatorEnter'
  }).run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/generatorTable.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/generatorTable.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddGeneratorTable(init) {
    _classCallCheck(this, AddGeneratorTable);

    this.server = init.server;
    this.selector = init.selector;
    this.colorCurrentPlus = '#15ae5e';
    this.colorCurrentMinus = '#f44336';
  }

  _createClass(AddGeneratorTable, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.selector));

      if (elem) {
        this.constructor.info();
        var init = {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          cache: 'default'
        };

        var calculate = function calculate(item) {
          var myMax = [];

          var conrt = _toConsumableArray(document.querySelectorAll(".mytable__".concat(item, "-group")));

          conrt.forEach(function (el) {
            return myMax.push(el.firstElementChild.textContent.trim());
          });
          var findMaxNumber = myMax.reduce(function (a, b) {
            return Math.max(a, b, 0);
          });
          myMax.forEach(function (el, i) {
            var result = el / findMaxNumber;

            var resultTarget = _toConsumableArray(document.querySelectorAll(".mytable__".concat(item, "-bar")))[i];

            resultTarget.style.transform = "scaleX(".concat(result, ")");
            if (result <= 0) resultTarget.style.borderColor = "".concat(_this.colorCurrentMinus);
          });
        };

        fetch("./".concat(this.server), init).then(function (response) {
          response.headers.get('Content-Type');
          return response.json();
        }).then(function (data) {
          Object.keys(data.process).map(function (i) {
            var sumAsis = _toConsumableArray(Object.values(data.process[i].asis)).reduce(function (a, b) {
              return a + b;
            }, 0);

            var sumTobe = _toConsumableArray(Object.values(data.process[i].tobe)).reduce(function (a, b) {
              return a + b;
            }, 0);

            var balance = 6;
            var markWarning = data.process[i].mark.warning;
            var validatWarning = markWarning >= -6 && markWarning <= 0 ? markWarning / balance : 0;
            var markCurrent = data.process[i].mark.current;
            var validatCurrent = markCurrent >= -6 && markCurrent <= 6 ? markCurrent / balance : 0;
            var validatCurrentColor = markCurrent > 0 ? _this.colorCurrentPlus : _this.colorCurrentMinus;
            var markPossibly = data.process[i].mark.possibly;
            var validatPossibly = markPossibly <= 6 && markPossibly >= 0 ? markPossibly / balance : 0;
            var container = "\n\t\t\t\t\t\t\t<div class=\"mytable__title\"><span class=\"mytable__title-count\">#".concat(+i + 1, "</span>").concat(data.process[i].name, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__group\">\n\t\t\t\t\t\t\t<div class=\"mytable__points\">\n\t\t\t\t\t\t\t<div class=\"mytable__points-item\">\n\t\t\t\t\t\t\t<span class=\"mytable__points-count\">").concat(data.process[i].mark.warning, "</span>\n\t\t\t\t\t\t\t<span class=\"mytable__points-bar\" style=\"transform: scaleX(").concat(-validatWarning, ");\">&nbsp;</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"mytable__points-item\">\n\t\t\t\t\t\t\t<span class=\"mytable__points-count\">").concat(data.process[i].mark.current, "</span>\n\t\t\t\t\t\t\t<span class=\"mytable__points-bar\" \n\t\t\t\t\t\t\tstyle=\"transform: scaleX(").concat(validatCurrent, "); border-color: ").concat(validatCurrentColor, ";\"\n\t\t\t\t\t\t\t>&nbsp;</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"mytable__points-item\">\n\t\t\t\t\t\t\t<span class=\"mytable__points-count\">").concat(data.process[i].mark.possibly, "</span>\n\t\t\t\t\t\t\t<span class=\"mytable__points-bar\" style=\"transform: scaleX(").concat(validatPossibly, ");\">&nbsp;</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis\">\n\t\t\t\t\t\t\t<div class=\"mytable__asis-group\">\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(sumAsis, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(data.process[i].asis.organizer, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(data.process[i].asis.administrator, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(data.process[i].asis.methodologist, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(data.process[i].asis.executor, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(data.process[i].asis.marketing, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__asis-item\">").concat(data.process[i].asis.purchases, "</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<span class=\"mytable__points-bar mytable__asis-bar\">&nbsp;</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe\">\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-group\">\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(sumTobe, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(data.process[i].tobe.organizer, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(data.process[i].tobe.administrator, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(data.process[i].tobe.methodologist, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(data.process[i].tobe.executor, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(data.process[i].tobe.marketing, "</div>\n\t\t\t\t\t\t\t<div class=\"mytable__tobe-item\">").concat(data.process[i].tobe.purchases, "</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<span class=\"mytable__points-bar mytable__tobe-bar\">&nbsp;</span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"mytable__status\">").concat(data.process[i].strategy, "</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t");
            var newContainer = document.createElement('DIV');
            newContainer.className = 'mytable__item';
            newContainer.innerHTML += container;
            elem.appendChild(newContainer);
          });
        }).then(function () {
          return calculate('asis');
        }).then(function () {
          return calculate('tobe');
        }).catch(function (error) {
          return console.log("Ouch! Fetch error: \n ".concat(error.message));
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddGeneratorTable;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2dlbmVyYXRvclRhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9jb25uZWN0LmpzXCIpO1xuIiwiLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4vLyAgICBOYW1lOiBpbmRleC5qc1xyXG4vLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4vLyBpbXBvcnQgQWRkTW92ZVNjcm9sbFVQIGZyb20gJy4vbW9kdWxlcy9tb3ZlU2Nyb2xsVVAnO1xyXG4vLyBpbXBvcnQgQWRkRmV0Y2hTZXJ2ZXJEYXRhIGZyb20gJy4vbW9kdWxlcy9mZXRjaFNlcnZlckRhdGEnO1xyXG5pbXBvcnQgQWRkR2VuZXJhdG9yVGFibGUgZnJvbSAnLi9tb2R1bGVzL2dlbmVyYXRvclRhYmxlJztcclxuXHJcbmNvbnN0IHN0YXJ0ID0gKCkgPT4ge1xyXG5cdGNvbnNvbGUubG9nKCdET006JywgJ0RPTUNvbnRlbnRMb2FkZWQnLCB0cnVlKTtcclxuXHJcblx0Ly8gbmV3IEFkZE1vdmVTY3JvbGxVUCgnLmpzX19tb3ZlU2Nyb2xsVVAnKS5ydW4oKTtcclxuXHJcblx0Ly8gbmV3IEFkZEZldGNoU2VydmVyRGF0YSgnLmpzX19nYWxsZXJ5RGF0YScsIHtcclxuXHQvLyBcdHNlcnZlcjogJ215LXNlcnZlci5qc29uJyxcclxuXHQvLyBcdHNlbGVjdG9yOiAnZGF0YS1nYWxsZXJ5LWluaXQnXHJcblx0Ly8gfSkucnVuKCk7XHJcblx0XHJcblx0bmV3IEFkZEdlbmVyYXRvclRhYmxlKHtcclxuXHRcdHNlcnZlcjogJ3NlcnZlci1oci1kYXRhLmpzb24nLFxyXG5cdFx0c2VsZWN0b3I6ICcuanNfX2dlbmVyYXRvckVudGVyJ1xyXG5cdH0pLnJ1bigpO1xyXG5cdFxyXG59O1xyXG5cclxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdyAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcikge1xyXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBzdGFydCgpLCBmYWxzZSk7XHJcbn1cclxuICIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkR2VuZXJhdG9yVGFibGUge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VydmVyID0gaW5pdC5zZXJ2ZXI7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMuY29sb3JDdXJyZW50UGx1cyA9ICcjMTVhZTVlJztcclxuXHRcdHRoaXMuY29sb3JDdXJyZW50TWludXMgPSAnI2Y0NDMzNic7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuc2VsZWN0b3J9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGluaXQgPSB7XHJcblx0XHRcdFx0bWV0aG9kOiAnR0VUJyxcclxuXHRcdFx0XHRoZWFkZXJzOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfSxcclxuXHRcdFx0XHRtb2RlOiAnY29ycycsXHJcblx0XHRcdFx0Y2FjaGU6ICdkZWZhdWx0J1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3QgY2FsY3VsYXRlID0gKGl0ZW0pID0+IHtcclxuXHRcdFx0XHRjb25zdCBteU1heCA9IFtdO1xyXG5cdFx0XHRcdGNvbnN0IGNvbnJ0ID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5teXRhYmxlX18ke2l0ZW19LWdyb3VwYCldO1xyXG5cdFx0XHRcdGNvbnJ0LmZvckVhY2goZWwgPT4gbXlNYXgucHVzaChlbC5maXJzdEVsZW1lbnRDaGlsZC50ZXh0Q29udGVudC50cmltKCkpKTtcclxuXHRcdFx0XHRjb25zdCBmaW5kTWF4TnVtYmVyID0gbXlNYXgucmVkdWNlKChhLCBiKSA9PiBNYXRoLm1heChhLCBiLCAwKSk7XHJcblxyXG5cdFx0XHRcdG15TWF4LmZvckVhY2goKGVsLCBpKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCByZXN1bHQgPSBlbCAvIGZpbmRNYXhOdW1iZXI7XHJcblx0XHRcdFx0XHRjb25zdCByZXN1bHRUYXJnZXQgPSBbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLm15dGFibGVfXyR7aXRlbX0tYmFyYCldW2ldO1xyXG5cdFx0XHRcdFx0cmVzdWx0VGFyZ2V0LnN0eWxlLnRyYW5zZm9ybSA9IGBzY2FsZVgoJHtyZXN1bHR9KWA7XHJcblx0XHRcdFx0XHRpZiAocmVzdWx0IDw9IDApIHJlc3VsdFRhcmdldC5zdHlsZS5ib3JkZXJDb2xvciA9IGAke3RoaXMuY29sb3JDdXJyZW50TWludXN9YDtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGZldGNoKGAuLyR7dGhpcy5zZXJ2ZXJ9YCwgaW5pdClcclxuXHRcdFx0XHQudGhlbigocmVzcG9uc2UpID0+IHtcclxuXHRcdFx0XHRcdHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKTtcclxuXHRcdFx0XHRcdHJldHVybiByZXNwb25zZS5qc29uKCk7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQudGhlbigoZGF0YSkgPT4ge1xyXG5cclxuXHRcdFx0XHRcdE9iamVjdC5rZXlzKGRhdGEucHJvY2VzcykubWFwKChpKSA9PiB7XHJcblx0XHRcdFx0XHRcdGNvbnN0IHN1bUFzaXMgPSBbLi4uT2JqZWN0LnZhbHVlcyhkYXRhLnByb2Nlc3NbaV0uYXNpcyldLnJlZHVjZSgoYSwgYikgPT4gYSArIGIsIDApO1xyXG5cdFx0XHRcdFx0XHRjb25zdCBzdW1Ub2JlID0gWy4uLk9iamVjdC52YWx1ZXMoZGF0YS5wcm9jZXNzW2ldLnRvYmUpXS5yZWR1Y2UoKGEsIGIpID0+IGEgKyBiLCAwKTtcclxuXHJcblx0XHRcdFx0XHRcdGNvbnN0IGJhbGFuY2UgPSA2O1xyXG5cdFx0XHRcdFx0XHRjb25zdCBtYXJrV2FybmluZyA9IGRhdGEucHJvY2Vzc1tpXS5tYXJrLndhcm5pbmc7XHJcblx0XHRcdFx0XHRcdGNvbnN0IHZhbGlkYXRXYXJuaW5nID0gbWFya1dhcm5pbmcgPj0gLTYgJiYgbWFya1dhcm5pbmcgPD0gMCA/IChtYXJrV2FybmluZyAvIGJhbGFuY2UpIDogMDtcclxuXHJcblx0XHRcdFx0XHRcdGNvbnN0IG1hcmtDdXJyZW50ID0gZGF0YS5wcm9jZXNzW2ldLm1hcmsuY3VycmVudDtcclxuXHRcdFx0XHRcdFx0Y29uc3QgdmFsaWRhdEN1cnJlbnQgPSBtYXJrQ3VycmVudCA+PSAtNiAmJiBtYXJrQ3VycmVudCA8PSA2ID8gKG1hcmtDdXJyZW50IC8gYmFsYW5jZSkgOiAwO1xyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0Y29uc3QgdmFsaWRhdEN1cnJlbnRDb2xvciA9IG1hcmtDdXJyZW50ID4gMCA/IHRoaXMuY29sb3JDdXJyZW50UGx1cyA6IHRoaXMuY29sb3JDdXJyZW50TWludXM7XHJcblxyXG5cdFx0XHRcdFx0XHRjb25zdCBtYXJrUG9zc2libHkgPSBkYXRhLnByb2Nlc3NbaV0ubWFyay5wb3NzaWJseTtcclxuXHRcdFx0XHRcdFx0Y29uc3QgdmFsaWRhdFBvc3NpYmx5ID0gbWFya1Bvc3NpYmx5IDw9IDYgJiYgbWFya1Bvc3NpYmx5ID49IDAgPyAobWFya1Bvc3NpYmx5IC8gYmFsYW5jZSkgOiAwO1xyXG5cclxuXHRcdFx0XHRcdFx0Y29uc3QgY29udGFpbmVyID0gYFxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX190aXRsZVwiPjxzcGFuIGNsYXNzPVwibXl0YWJsZV9fdGl0bGUtY291bnRcIj4jJHsraSArIDF9PC9zcGFuPiR7ZGF0YS5wcm9jZXNzW2ldLm5hbWV9PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX2dyb3VwXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX3BvaW50c1wiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX19wb2ludHMtaXRlbVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXl0YWJsZV9fcG9pbnRzLWNvdW50XCI+JHtkYXRhLnByb2Nlc3NbaV0ubWFyay53YXJuaW5nfTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cIm15dGFibGVfX3BvaW50cy1iYXJcIiBzdHlsZT1cInRyYW5zZm9ybTogc2NhbGVYKCR7LXZhbGlkYXRXYXJuaW5nfSk7XCI+Jm5ic3A7PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX19wb2ludHMtaXRlbVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXl0YWJsZV9fcG9pbnRzLWNvdW50XCI+JHtkYXRhLnByb2Nlc3NbaV0ubWFyay5jdXJyZW50fTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cIm15dGFibGVfX3BvaW50cy1iYXJcIiBcclxuXHRcdFx0XHRcdFx0XHRzdHlsZT1cInRyYW5zZm9ybTogc2NhbGVYKCR7dmFsaWRhdEN1cnJlbnR9KTsgYm9yZGVyLWNvbG9yOiAke3ZhbGlkYXRDdXJyZW50Q29sb3J9O1wiXHJcblx0XHRcdFx0XHRcdFx0PiZuYnNwOzwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fcG9pbnRzLWl0ZW1cIj5cclxuXHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzcz1cIm15dGFibGVfX3BvaW50cy1jb3VudFwiPiR7ZGF0YS5wcm9jZXNzW2ldLm1hcmsucG9zc2libHl9PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXl0YWJsZV9fcG9pbnRzLWJhclwiIHN0eWxlPVwidHJhbnNmb3JtOiBzY2FsZVgoJHt2YWxpZGF0UG9zc2libHl9KTtcIj4mbmJzcDs8L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX2FzaXNcIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fYXNpcy1ncm91cFwiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX19hc2lzLWl0ZW1cIj4ke3N1bUFzaXN9PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX2FzaXMtaXRlbVwiPiR7ZGF0YS5wcm9jZXNzW2ldLmFzaXMub3JnYW5pemVyfTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX19hc2lzLWl0ZW1cIj4ke2RhdGEucHJvY2Vzc1tpXS5hc2lzLmFkbWluaXN0cmF0b3J9PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX2FzaXMtaXRlbVwiPiR7ZGF0YS5wcm9jZXNzW2ldLmFzaXMubWV0aG9kb2xvZ2lzdH08L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fYXNpcy1pdGVtXCI+JHtkYXRhLnByb2Nlc3NbaV0uYXNpcy5leGVjdXRvcn08L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fYXNpcy1pdGVtXCI+JHtkYXRhLnByb2Nlc3NbaV0uYXNpcy5tYXJrZXRpbmd9PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX2FzaXMtaXRlbVwiPiR7ZGF0YS5wcm9jZXNzW2ldLmFzaXMucHVyY2hhc2VzfTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXl0YWJsZV9fcG9pbnRzLWJhciBteXRhYmxlX19hc2lzLWJhclwiPiZuYnNwOzwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fdG9iZVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX190b2JlLWdyb3VwXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX3RvYmUtaXRlbVwiPiR7c3VtVG9iZX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fdG9iZS1pdGVtXCI+JHtkYXRhLnByb2Nlc3NbaV0udG9iZS5vcmdhbml6ZXJ9PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cIm15dGFibGVfX3RvYmUtaXRlbVwiPiR7ZGF0YS5wcm9jZXNzW2ldLnRvYmUuYWRtaW5pc3RyYXRvcn08L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fdG9iZS1pdGVtXCI+JHtkYXRhLnByb2Nlc3NbaV0udG9iZS5tZXRob2RvbG9naXN0fTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX190b2JlLWl0ZW1cIj4ke2RhdGEucHJvY2Vzc1tpXS50b2JlLmV4ZWN1dG9yfTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX190b2JlLWl0ZW1cIj4ke2RhdGEucHJvY2Vzc1tpXS50b2JlLm1hcmtldGluZ308L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwibXl0YWJsZV9fdG9iZS1pdGVtXCI+JHtkYXRhLnByb2Nlc3NbaV0udG9iZS5wdXJjaGFzZXN9PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PHNwYW4gY2xhc3M9XCJteXRhYmxlX19wb2ludHMtYmFyIG15dGFibGVfX3RvYmUtYmFyXCI+Jm5ic3A7PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteXRhYmxlX19zdGF0dXNcIj4ke2RhdGEucHJvY2Vzc1tpXS5zdHJhdGVneX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRgO1xyXG5cdFx0XHRcdFx0XHRjb25zdCBuZXdDb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdESVYnKTtcclxuXHRcdFx0XHRcdFx0bmV3Q29udGFpbmVyLmNsYXNzTmFtZSA9ICdteXRhYmxlX19pdGVtJztcclxuXHRcdFx0XHRcdFx0bmV3Q29udGFpbmVyLmlubmVySFRNTCArPSBjb250YWluZXI7XHJcblx0XHRcdFx0XHRcdGVsZW0uYXBwZW5kQ2hpbGQobmV3Q29udGFpbmVyKTtcclxuXHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdC50aGVuKCgpID0+IGNhbGN1bGF0ZSgnYXNpcycpKVxyXG5cdFx0XHRcdC50aGVuKCgpID0+IGNhbGN1bGF0ZSgndG9iZScpKVxyXG5cdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhgT3VjaCEgRmV0Y2ggZXJyb3I6IFxcbiAke2Vycm9yLm1lc3NhZ2V9YCkpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0JBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBOENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUF0SEE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQVZBO0FBQ0E7QUFEQTtBQUFBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=